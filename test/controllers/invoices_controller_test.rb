require "test_helper"

class InvoicesControllerTest < ActionDispatch::IntegrationTest
  test "should get download" do
    get invoices_download_url
    assert_response :success
  end
end
