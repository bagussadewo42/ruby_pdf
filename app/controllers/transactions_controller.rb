class TransactionsController < ApplicationController

  def show
    render 'show'
  end

  def download
    layout = 'layouts/transactions-layout.html.haml'
    template = 'transactions/transactions-print.html.haml'
    disposition = params[:disposition]
    pdf_content = render_to_string(page_size: 'A4', template: template, layout: layout)
    pdf = to_pdf(pdf_content)
    send_data pdf, filename: 'Transaksi.pdf', type: 'application/pdf', disposition: disposition
  end

  def to_pdf(pdf_content)
    WickedPdf.new.pdf_from_string(
      pdf_content,
      # zoom: 0.78125
      # zoom: 1.28
      # margin: { top: 1, left: '0.5cm', bottom: 0, right: '0.5cm' }
    )
  end

end
